const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'vuestore'
});

connection.connect((err) => {
    if (err) {
        console.error(`Error connecting ${err.stack}`);
        return;
    }
    console.log(`Connected as id ${connection.threadId}`);
})

connection.on('error', err => {
    console.log("[mysql error]",err);
});

module.exports = {
    connection: connection,
    secret: 'secret'
};