const express = require('express');
const app = express.Router();
const connection = require('../config').connection;
const secret = require('../config').secret;
const bcrypt = require('bcryptjs');
const passport = require('passport');
const jwt = require('jsonwebtoken');

const table = 'users';

app.post('/register', (req, res) => {
    const user = req.body;
    getUserByUsername(user.username, response => {
        if (response.length > 0) {
            res.send({success: false, msg: 'User with this username already registered.'});
        } else {
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(user.password, salt, (err, hash) => {
                    if (err) throw err;
                    const query = `INSERT INTO ${table} VALUES (
                        NULL, 
                        '${user.fullname}', 
                        '${user.username}', 
                        '${user.email}', 
                        '${hash}'
                    )`;
                
                    connection.query(query, (err, result) => {
                        if (err) throw err;
                        res.json({success: true, msg: 'User succesfully registered.'});
                    });
                })
            });
        }
    });
    
});

app.post('/login', (req, res) => {
    const user = req.body;
    getUserByUsername(user.username, response => {
        if (response.length > 0) {
            const foundUser = response[0];
            comparePassword(user.password, foundUser.password, (err, isMatch) => {
                if (err) throw err;
                if (isMatch) {
                    const token = jwt.sign(JSON.stringify(foundUser), secret);
                    res.json({
                        success: true,
                        msg: `Logged as <b>${foundUser.username}</b>.`,
                        token: `JWT ${token}`,
                        user: {
                            id: foundUser.id,
                            fullname: foundUser.name,
                            username: foundUser.username,
                            email: foundUser.email
                        }
                    });
                } else {
                    return res.send({success: false, msg: 'Password is incorrect.'});
                }
            });
        } else {
            res.send({success: false, msg: 'User not found.'});
        }
    });
});

getUserByUsername = (username, callback) => {
    const query = `SELECT * FROM ${table} WHERE username = '${username}'`;
    connection.query(query, (err, foundUser) => {
        if (err) throw err;
        callback(foundUser);
    });
}

comparePassword = (candidatePassword, hash, callback) => {
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
}

module.exports = app;
