const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const connection = require('./config').connection
const app = express()
const items = require('./routes/items')
const users = require('./routes/users')
const passport = require('passport')
const path = require('path')

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.use(passport.initialize())
app.use(passport.session())

app.use('/src/uploads', express.static(__dirname + '/uploads'))
require('./passport')(passport)

app.use('/items', items)
app.use('/users', users)
app.get('/', (req, res) => {
    res.send('Invalid endpoint')
})

app.listen(process.env.PORT || 8080)