module.exports = {
    devServer: {
      host: 'localhost',
      port: 8081
    },
    css: {
      loaderOptions: {
        sass: {
          data: `
            @import "@/scss/_global.scss";
          `
        }
      }
    }
  };