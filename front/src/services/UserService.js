import Api from '@/services/Api'
const jwtDecode = require('jwt-decode');

export default {
    registerUser(user) {
        return Api().post('/users/register', user);
    },
    userLogin(user) {
        return Api().post('/users/login', user);
    },
    logout() {
        localStorage.clear();
    },
    storeUserData(token) {
        localStorage.setItem('user_token', token);
    },
    decodeToken(token) {
        return jwtDecode(token);
    }
}