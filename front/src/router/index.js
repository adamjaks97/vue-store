import Vue from 'vue';
import Router from 'vue-router';
import BootstrapVue from 'bootstrap-vue';

import Feed from '@/components/Feed';
import Register from '@/components/Register';
import Login from '@/components/Login';
import Profile from '@/components/Profile';
import Cart from '@/components/Cart';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(Router);
Vue.use(BootstrapVue);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Feed
        },
        {
            path: '/register',
            component: Register
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/profile',
            component: Profile
        },
        {
            path: '/cart',
            component: Cart
        }
    ]
});
