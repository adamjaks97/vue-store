export default (value) => {
    return value.slice(0, 140) + '...';
}