import Vue from 'vue'
import App from './App.vue'
import Router from '@/router/index'
import { store } from './store/store'
import truncate from '@/filters/truncate'

Vue.filter('truncate', truncate)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: Router,
  store: store
}).$mount('#app')
