const express = require('express')
const app = express.Router()
const connection = require('../config').connection
const multer = require('multer')

const table = 'items'

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './src/uploads')
    },
    filename: (req, file, callback) => {
        callback(null, file.originalname)
    }
});

const fileFilter = (req, file, callback) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true)
    } else {
        callback(null, false)
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter,
});

app.get('/fetch/:categoryId/:username', (req, res) => {

    let query = `SELECT * FROM categories_items ci
    INNER JOIN ${table} i ON i.id = ci.item_id
    WHERE ci.category_id = '${req.params.categoryId}' `

    if (req.params.username !== 'undefined') {
        query += `&& i.author = '${req.params.username}'`
    }

    connection.query(query, (err, data) => {
        if (err) throw err
        res.send(data)
    });
});

app.get('/categories', (req, res) => {
    const query = `SELECT * FROM categories`
    connection.query(query, (err, data) => {
        if (err) throw err
        res.send(data)
    });
});

app.post('/add', upload.single('product-image'), (req, res) => {
    const query = `INSERT INTO ${table} VALUES (
        NULL, 
        '${req.body.title}',
        '${req.body.description}',
        '${req.body.price}',
        '${req.body.author}',
        '${req.body.author_id}',
        'http://localhost:8080/src/uploads/${req.file.filename}'
    )`;

    connection.query(query, (err, data) => {
        if (err) {
            
            res.send({ success: false, msg: err })
            throw err
        }
        
        let values = Array.from(req.body.categories).map((categoryId) => {
            return [ 'NULL', data.insertId, categoryId ]
        });

        connection.query(`INSERT INTO categories_items VALUES ?`, [values], (err, data) => {
            if (err) {
                res.send({ success: false, msg: err })
                throw err
            }
            res.send({ success: true, msg: 'Item successfully added'})
        });
    });
});

app.post('/buy', (req,res) => {
    let values = Array.from(req.body)
    connection.query(`INSERT INTO solditems VALUES ?`, [values], (err, data) => {
        if (err) {
            res.send({ success: false, msg: err })
            throw err
        }
        res.send({ success: true, msg: 'Items bought'})
    })
})

app.get('/get-sold/:id', (req, res) => {
    connection.query(`
        SELECT * FROM solditems INNER JOIN items 
        ON solditems.item_id = items.id WHERE solditems.seller_id = ${req.params.id}`, (err, data) => {
        if (err) {
            res.send({ success: false, msg: err })
            throw err
        }
        res.send({
            success: true,
            msg: 'Successfully fetch sold items',
            data: data
        })
    })
})

app.get('/get-bought/:id', (req, res) => {
    connection.query(`
        SELECT * FROM solditems INNER JOIN items 
        ON solditems.item_id = items.id WHERE buyer_id = ${req.params.id}`, (err, data) => {
        if (err) {
            res.send({ success: false, msg: err })
            throw err
        }
        res.send({
            success: true,
            msg: 'Successfully fetch bought items',
            data: data
        })
    })
})
module.exports = app