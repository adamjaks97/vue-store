import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    user: localStorage.getItem("user") ? localStorage.getItem("user") : null,
    alertMessage: "",
    alert: {
      message: "",
      variant: "",
      show: 0
    },
    cartAmount: localStorage.getItem("cartAmount")
      ? localStorage.getItem("cartAmount")
      : 0,
    cartItems: localStorage.getItem("items")
      ? JSON.parse(localStorage.getItem("items"))
      : [],
    totalPrice: localStorage.getItem("totalPrice")
      ? JSON.parse(localStorage.getItem("totalPrice"))
      : 0
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    showAlert(state, config) {
      state.alert = {
        message: config.message,
        variant: config.variant,
        show: 1
      };
    },
    addToCart(state, item) {
      item["amountInCart"] = 1
      state.cartItems.push(item)
      state.cartAmount++
      state.totalPrice += item.price

      localStorage.removeItem("items")
      localStorage.setItem("items", JSON.stringify(state.cartItems))
      localStorage.removeItem("cartAmount")
      localStorage.setItem("cartAmount", state.cartAmount)
      localStorage.removeItem("totalPrice")
      localStorage.setItem("totalPrice", state.totalPrice)
    },
    deleteCartItem(state, item) {
      let index = state.cartItems
        .map(el => {
          return el.id
        })
        .indexOf(item.id)
      state.totalPrice -= state.cartItems[index].price
      state.cartItems.splice(index, 1)
      
      state.cartAmount--
      localStorage.removeItem("items")
      localStorage.setItem(
        "items",
        JSON.stringify(state.cartItems)
      )
      localStorage.removeItem("cartAmount")
      localStorage.setItem(
        "cartAmount",
        JSON.stringify(state.cartAmount)
      )
      localStorage.removeItem('totalPrice')
      localStorage.setItem('totalPrice', state.totalPrice)
    },
    increaseItemAmount(state, itemId) {
      state.cartItems.forEach(el => {
        if (el.id === itemId) {
          el.amountInCart++
          state.totalPrice += el.price
        }
      })
    },
    decreaseItemAmount(state, itemId) {
      state.cartItems.forEach(el => {
        if (el.id === itemId) {
          if (el.amountInCart > 1) {
            el.amountInCart--
            state.totalPrice -= el.price
          } else {
            this.commit('deleteCartItem', el)
          }
        }
      })
    }
  }
});
