import Api from '@/services/Api'

export default {
    fetchItems(categoryId, username) {
        return Api().get(`items/fetch/${categoryId}/${username}`)
    },
    addItem(formData) {
        return Api().post('items/add', formData, { 'content-type': 'multipart/form-data' })
    },
    getCategories() {
        return Api().get('items/categories')
    },
    toOrderItems(soldItems, user) {
        let data = Array.from(soldItems).map(item => {
            return [ 'NULL', item.id, item.author_id, user.id, new Date() ]
        })
        return Api().post('items/buy', data)
    },
    getSoldItems(userId) {
        return Api().get(`items/get-sold/${userId}`)
    },
    getBoughtItems(userId) {
        return Api().get(`items/get-bought/${userId}`)
    }
} 